 #!/usr/bin/python3

import os
import csv
import itertools
import random
import os
import argparse

secrets_path = '/etc/communistquotes/secrets/'

parser = argparse.ArgumentParser(description='Parse quotes from .csv files, and post a random quote to Mastodon API')
parser.add_argument('--debug', action='store_true', help='Dont actually login or post to the remote API')
args = parser.parse_args()

mastodon_api = None
if not args.debug:
    from mastodon import Mastodon

    instance = os.environ['MASTODON_INSTANCE']
    username = os.environ['MASTODON_USERNAME']

    # Create application if it does not exist
    if not os.path.isfile(secrets_path + instance + '.secret'):
        if Mastodon.create_app(
            'tootbot',
            api_base_url = 'https://' + instance,
            to_file = secrets_path + instance + '.secret'
        ):
            print('tootbot app created on instance ' + instance)
        else:
            print('failed to create app on instance ' + instance)
            exit(1)

    mastodon_api = Mastodon(
        client_id    = secrets_path + instance + '.secret',
        api_base_url = 'https://' + instance
    )
    mastodon_api.log_in(
        username = username,
        password = os.environ['MASTODON_PASSWORD'],
        scopes   = ['read', 'write'],
        to_file  = secrets_path + username + ".secret"
    )

quotes = []
for root, dirs, files in os.walk('quotes/'):
    for name in files:
        (base, ext) = os.path.splitext(name)
        if ext == '.csv':
            full_name = os.path.join(root, name)
            with open(full_name) as csvfile:
                csvreader = csv.DictReader(csvfile, delimiter=',', quotechar='`', skipinitialspace=True)
                quotes += list(csvreader)

for q in quotes:
    # Check correct number of fields
    assert (len(q) == 4),                                                           "Error: wrong number of fields in " + str(q)
    # Check correct field names
    assert ('quote' in q and 'author' in q and 'link' in q and 'linktitle' in q),   "Error: missing field in " + str(q)
    # Check that no field is empty
    assert (q['quote'] and q['author'] and q['link'] and q['linktitle']),           "Error: empty field in " + str(q)

print(f"Found {len(quotes)} total quotes")

row = random.choice(quotes)
text = f"<p>{row['quote']}</p>&nbsp;&nbsp;&nbsp;&nbsp;- {row['author']}, <a href={row['link']}>{row['linktitle']}</a>"
print(text)

if not args.debug:
    mastodon_api.status_post(text, visibility='public', content_type='text/html')
