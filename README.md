# This project has moved to https://yerbamate.dev/nutomic/communistquotes


# Communist Quotes Bot

A Fediverse bot that posts random quotes from different communists.

You can check it out here: https://impenetrable.fortress.promo/communistquotes

## Contribute Quotes

Edit the file [communistquotes/marxistquotes.csv](communistquotes/marxistquotes.csv) and
make an MR.

## Compile and Debug

To run the bot,
execute the following commands:

```bash
$ cd communistquotes/
$ sudo docker build . -f src/Dockerfile -t communistquotes
$ sudo docker run -it --rm --name communistquotes communistquotes --debug
```

## Deploy

You need a Mastodon/Pleroma account where the bot will post, and ssh access to a server where the Python bot will run. This can both running on the same server, or they can be seperate.

First, copy `env.example` to `env`, then enter the login information for your Mastodon/Pleroma bot account. Copy `inventory.example` to `inventory`, and enter your server's ssh address. Then run the following command:
```
$ ansible-playbook ansible.yml --become
```